package br.com.bossini.pessoal_test_running_ci_on_gitlab_after_creating_file_manually;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PessoalTestRunningCiOnGitlabAfterCreatingFileManuallyApplication {

    public static void main(String[] args) {
        SpringApplication.run(PessoalTestRunningCiOnGitlabAfterCreatingFileManuallyApplication.class, args);
    }

}
